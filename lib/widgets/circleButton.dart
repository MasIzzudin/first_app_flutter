import 'package:flutter/material.dart';


class CircleButton extends StatelessWidget {
  final Function onRequest;
  final Text title;

  CircleButton({ this.onRequest, this.title });

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: onRequest,
      child: title,
    );
  }
}
