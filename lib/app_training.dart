import 'package:flutter/material.dart';
import 'package:first_app/widgets/circleButton.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int number = 0;

  // void onRequest() {

  // }

  void onReset() {
    setState(() {
      number = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('My First Application Flutter'),
          ),
          body: Container(
            // color: Colors.yellow,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    stops: [
                  0.1,
                  0.5,
                ],
                    colors: [
                  Colors.red,
                  Colors.yellow,
                ])),
            // margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
            margin: EdgeInsets.only(right: 10, left: 10),
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Center(
                    child: Container(
                        color: Colors.lightBlue,
                        // width: 150,
                        // height: 50,
                        child: Text(
                          'Hello World ajskjaksjkajskajskjaskjaksjkaj',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.red,
                              fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.w700,
                              fontSize: 20),
                        ))),
                Text(
                  '$number',
                  style: TextStyle(fontSize: 15),
                ),
                Row(
                  children: <Widget>[Text('Satu '), Text('dua')],
                ),
                CircleButton(
                  onRequest: () {
                    setState(() {
                      number = number + 1;
                    });
                  },
                  title: Text('ini Button'),
                ),
                CircleButton(
                  onRequest: onReset,
                  title: Text('Reset'),
                )
              ],
            ),
          )),
    );
  }
}
