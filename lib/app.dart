import 'package:first_app/ui/view/second_screen.dart';
import 'package:flutter/material.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Builder(
        builder: (context) => Scaffold(
            appBar: AppBar(
              title: Text('My First Application Flutter'),
            ),
            body: Center(
              child: RaisedButton(
                child: Text("Login"),
                onPressed: () {
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
                    return SecondScreen();
                  }));
                },),
            )),
      ),
    );
  }
}
