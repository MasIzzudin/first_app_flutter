import 'package:flutter/material.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<Widget> products = [];
  int number = 0;
  String nameProduct = "";
  var txt = TextEditingController();

  void _changeTextInput(text) {
    setState(() {
      nameProduct = text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('My First Application Flutter'),
          ),
          body: Container(
            child: ListView(
              children: <Widget>[
                BottomAppBar(
                  child: TextField(
                    controller: txt,
                    onChanged: (text) => _changeTextInput(text),
                    autocorrect: false,
                    decoration: InputDecoration(
                        fillColor: Colors.red,
                        suffixIcon: IconButton(
                          icon: Icon(Icons.add),
                          onPressed: () {
                            setState(() {
                              products.add(Text(
                                '$nameProduct',
                                style: TextStyle(fontSize: 35),
                              ));
                              txt.clear();
                            });
                          },
                        ),
                        contentPadding: EdgeInsets.only(left: 15),
                        border: InputBorder.none,
                        hintText: 'Enter Your Product'),
                  ),
                ),
                Column(
                  children: products,
                )
              ],
            ),
          )),
    );
  }
}
